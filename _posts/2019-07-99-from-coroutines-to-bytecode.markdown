---
layout: post
title:  "Difference between Kotlin couroutine's Globalscope and lauch"
date:   2019-07-23 20:10:36 +0200
categories: jekyll update
published: false
---

<script src="https://unpkg.com/kotlin-playground@1"></script>

<script>
document.addEventListener('DOMContentLoaded', function() {
  KotlinPlayground('.code-blocks-selector');
});
</script>


# Difference between GlobalScope and launch ##

Both coroutine builders `GlobalScope.launch {}` and `launch {}` are used to start a new coroutine. 
`GlobalScope.launch {}` lauches a coroutine in the 'global' scope and `launch {}` starts a coroutine in
a `CoroutineScope`. But what does this mean? 

`runBlocking {}` will block execution untill all coroutines inside it's block have been completed. For example in the following snippet, the line at `4.` will only print when both coroutines defined inside `runBlocking` have been completed:


```kotlin
fun main() {
    println("1. Let's begin")
    runBlocking {
        launch { 
            delay(1000)
            println("3. child ONE")
        }
        
        launch { 
            delay(500)
            println("2. child TWO")
        }
    }
    
    println("4. Only when the children inside runBlocking complete, execution follows on this line")
}
```

[try it][runblocking-try-it]






## Pipelines



Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: https://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
[runblocking-try-it]: https://try.kotlinlang.org/#/UserProjects/ddsr38s4bjvt00cr7bagcebibj/1ejmadk62s9k2v6352ona5aq3f


## Java Bytecode