---
layout: post
title:  "Difference between Kotlin Globalscope.launch and lauch"
date:   2019-07-23 20:10:36 +0200
categories: kotlin coroutines
---


# Difference between GlobalScope.launch and launch ##

Both coroutine builders `GlobalScope.launch {}` and `launch {}` are used to start a new coroutine. `GlobalScope.launch {}` starts a new coroutine in the 'global' scope and `launch {}` starts a new coroutine in a `CoroutineScope`. But what does this mean? 

To understand what is happening, we take a look at `runBlocking {}`. `runBlocking {}` will block execution (executing the next line of code after `runBlocking {}`) until all coroutines defined inside it's [scope][scope-vs-context] have been completed. For example in the following snippet, the line at `4.` will only execute when both coroutines defined inside `runBlocking {}` have been completed:


```kotlin
fun main() {
    println("1. Let's begin")
    runBlocking {
        launch { 
            delay(1000)
            println("3. coroutine ONE")
        }
        
        launch { 
            delay(500)
            println("2. coroutine TWO")
        }
    }
    
    println("4. Only when the children inside runBlocking complete, execution follows on this line")
}
```

[try it][runblocking-launch-try-it]


Let try running the same code with `GlobalScope.launch {}` instead of `launch {}`:

```kotlin
fun main() {
    println("1. Let's start with GlobalScope.launch {}")
    runBlocking {
        GlobalScope.launch {
            delay(1000)
            println("3. coroutine ONE")
        }

        GlobalScope.launch {
            delay(100)
            println("2. coroutine TWO")
        }
    }

    println("4. This line will execute even if the coroutines inside runBlocking did not complete.")
}
```
[try it][runblocking-globalscope-launch-try-it]

Now the coroutines inside `runBlocking {}`'s scope did not complete, and execution continued. What is happening here?

`runBlocking {}` defines a `CoroutineScope` where coroutines run in. However, the coroutines launched in the above example are running in a separate 'global' scope, where `runBlocking` has no control over. As far as `runBlocking {}` knows, there are no coroutines running inside it's scope. 



[runblocking-launch-try-it]: https://try.kotlinlang.org/#/UserProjects/ddsr38s4bjvt00cr7bagcebibj/1ejmadk62s9k2v6352ona5aq3f
[runblocking-globalscope-launch-try-it]: https://try.kotlinlang.org/#/UserProjects/t04ej0qngr4avtc3k09fc69gck/bn6ggp2ttmhig5unbagqlj8luq
[scope-vs-context]: [https://medium.com/@elizarov/coroutine-context-and-scope-c8b255d59055]

